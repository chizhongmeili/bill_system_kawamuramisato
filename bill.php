<?php
$JSON_POST = json_decode(file_get_contents("php://input"), true);
$response_list = [];
$db_link = getDBLink();

////// 校舎名の取得 /////
if ($JSON_POST["actionKind"] === "school") {
    $response_list = findSchool($db_link);
//// 生徒情報の取得 ////
} else if ($JSON_POST["actionKind"] === "find") {
    $response_list = findStudent($db_link, $JSON_POST);
//// ステータスの取得 ////
 } else if ($JSON_POST["actionKind"] === "status") {
    $response_list = findStatus($db_link, $JSON_POST);
 }

mysqli_close($db_link);
responseJson($response_list);

exit;

//// 請求一覧の取得 ////
function findStudent($db_link, $search_info) {
    $studentList = [];
    $where = [];

    $query = " SELECT "
            ." amount_table.bill_id, "
            ." student_table.student_id, "
            ." school_master.school, "
            ." amount_table.bill_number, "
            ." student_table.last_kanji, "
            ." student_table.first_kanji, "
            ." amount_table.total_amount, "
            ." amount_table.basic_amount, "
            ." amount_table.option_amount, "
            ." status_master.status, "
            ." classification_master.classification "
            ." FROM student_table "
            ." JOIN school_master ON student_table.school = school_master.school_id "
            ." JOIN classification_master ON student_table.classification = classification_master.classification_id "
            ." JOIN amount_table ON student_table.student_id = amount_table.student_id "
            ." JOIN status_master ON amount_table.status = status_master.status_id ";
    
    if (isset($search_info["studentName"]) && $search_info["studentName"] != null) {
        $where[] = " concat(student_table.last_kanji,student_table.first_kanji) like '%".$search_info["studentName"]."%'";
    } 
    if (isset($search_info["studentSchool"]) && $search_info["studentSchool"] != null && $search_info["studentSchool"] != '選択してください') {
        $where[] = " school_master.school_id = '" . $search_info["studentSchool"] . "'";
    }
    if (isset($search_info["statusKind"]) && $search_info["statusKind"] != null && $search_info["statusKind"] != '選択してください') {
        $where[] = " status_master.status_id = '" . $search_info["statusKind"] . "'";
    }
    
    for ($i = 0; $i < count($where); $i++) {
        $value = $where[$i];
        if ($i === 0) {
            $query = $query. ' WHERE ' .$value;
        } else if ($i >= 1) {
            $query = $query. ' AND ' .$value;
        }
    }   
    $query = $query." ORDER BY student_table.student_id ASC;";
    // print $query;
    $result = mysqli_query($db_link,$query);

    while ($row = mysqli_fetch_array($result)) {
        $rowmap = [];
        
        $rowmap['bill_id'] = $row["bill_id"];
        $rowmap['student_id'] = $row["student_id"];
        $rowmap['classification'] = $row["classification"];
        $rowmap['school'] = $row["school"];
        $rowmap['bill_number'] = $row["bill_number"];
        $rowmap['last_kanji'] = $row["last_kanji"];
        $rowmap['first_kanji'] = $row["first_kanji"];
        $rowmap['status'] = $row["status"];
        $rowmap['total_amount'] = $row["total_amount"];
        $rowmap['basic_amount'] = $row["basic_amount"];
        $rowmap['option_amount'] = $row["option_amount"];

        $studentList[] = $rowmap;
    }
    mysqli_free_result($result);
    return $studentList;
}

///// 校舎の取得 /////
function findSchool($db_link) {
    $query = " SELECT school_id, school FROM school_master;";
    $result = mysqli_query($db_link,$query);
    
    while ($row = mysqli_fetch_array($result)) {
        $rowMap = [];
        $rowMap["school_id"] = $row["school_id"];
        $rowMap["school"] = $row["school"];
        $schoolList[] = $rowMap;
        
    }
    mysqli_free_result($result);

    return $schoolList;
}

//// ステータスの取得 ////
function findStatus($db_link) {
    $query =" SELECT status_id, status FROM status_master;";
    $result = mysqli_query($db_link,$query);
    while ($row = mysqli_fetch_array($result)) {
        $rowMap = [];
        $rowMap["status_id"] = $row["status_id"];
        $rowMap["status"] = $row["status"];
        $schoolList[] = $rowMap;
    }
    mysqli_free_result($result);
    return $schoolList;
}

///// DBと接続 /////
function getDBLink() {
    $host = "localhost";
    $userName = "root";
    $passWord = "";
    $dbName = "bill_system";
    $db_link = mysqli_connect($host, $userName, $passWord, $dbName);
    mysqli_set_charset($db_link, 'utf8');
    // 接続状況をチェックします
    if (mysqli_connect_errno()) {
        die("データベースに接続できません:" . mysqli_connect_error() . "\n");
    }
    return $db_link;
}
///// JSON形式に変換して返す ////
function responseJson($list) {
    header("Content-type: application/json; charset=UTF-8");
    echo json_encode($list);
}


?>