<?php
$JSON_POST = json_decode(file_get_contents("php://input"), true);
$response_list = [];
$db_link = getDBLink();

/// 校舎の取得 ///
if ($JSON_POST["actionKind"] === "school") {
    $response_list = findSchool($db_link);
} 
/// 生徒名の取得 ///
else if ($JSON_POST["actionKind"] === "student") {
    $response_list = findStudent($db_link, $JSON_POST);
}
/// 生徒情報の保存 ///
else if ($JSON_POST["actionKind"] === "save") {
      saveStudent($db_link, $JSON_POST);
}


mysqli_close($db_link);
responseJson($response_list);

///// 校舎の取得 /////
function findSchool($db_link) {
    $query = " SELECT school_id, school FROM school_master;";
    $result = mysqli_query($db_link,$query);
    
    while ($row = mysqli_fetch_array($result)) {
        $rowMap = [];
        $rowMap["school_id"] = $row["school_id"];
        $rowMap["school"] = $row["school"];
        
        $schoolList[] = $rowMap;
    }
    mysqli_free_result($result);

    return $schoolList;
}

////// 生徒名の取得 //////
function findStudent($db_link, $JSON_POST) {
    $studentList = [];
    // $where = [];

    $query = " SELECT "
            ." student_table.student_id, "
            ." student_table.last_kanji, "
            ." student_table.first_kanji, "
            ." student_table.benefit, "
            ." student_table.max_limit, "
            ." student_table.classification, "
            ." school_master.school, "
            ." amount_table.bill_id, "
            ." amount_table.bill_number, "
            ." amount_table.total_amount, "
            ." amount_table.basic_amount, "
            ." amount_table.option_amount, "
            ." amount_table.plan1, "
            ." amount_table.plan2, "
            ." amount_table.plan3, "
            ." status_master.status "
            ." FROM student_table "
            ." JOIN school_master ON student_table.school = school_master.school_id "
            ." JOIN amount_table ON student_table.student_id = amount_table.student_id "
            ." JOIN status_master ON amount_table.status = status_master.status_id ";

            if(isset($JSON_POST['school']) && $JSON_POST['school'] != null) {
                $query = $query. "WHERE student_table.school = ". $JSON_POST['school'];
            }
            if(isset($JSON_POST['id']) && $JSON_POST['id'] != null) {
                $query = $query. "WHERE student_table.student_id = ". $JSON_POST['id'];
            }

    $query = $query. " ORDER BY student_id ASC;";
            // print $query;
    $result = mysqli_query($db_link,$query);
    while ($row = mysqli_fetch_array($result)) {
        $rowmap = [];
        $rowmap['id'] = $row["student_id"];
        $rowmap['school'] = $row["school"];
        $rowmap['last_name'] = $row["last_kanji"];
        $rowmap['first_name'] = $row["first_kanji"];
        $rowmap['classification'] = $row["classification"];
        $rowmap['benefit'] = $row["benefit"];
        $rowmap['max_limit'] = $row["max_limit"];
        $rowmap['bill_id'] = $row["bill_id"];
        $rowmap['bill_number'] = $row["bill_number"];
        $rowmap['total_amount'] = $row["total_amount"];
        $rowmap['basic_amount'] = $row["basic_amount"];
        $rowmap['option_amount'] = $row["option_amount"];
        $rowmap['plan1'] = $row["plan1"];
        $rowmap['plan2'] = $row["plan2"];
        $rowmap['plan3'] = $row["plan3"];
        $rowmap['status'] = $row["status"];
       
        
        $studentList[] = $rowmap;
    }    
    mysqli_free_result($result);
    return $studentList;
}

///// 生徒情報の保存 /////
function saveStudent($db_link, $JSON_POST){
    $query = "";
    $query .=" UPDATE amount_table ";
    $query .=" SET ";
    $query .=" status = " .$JSON_POST['status'] ;
    $query .=" WHERE student_id = " .$JSON_POST['id']. ";";
    $result = mysqli_query($db_link, $query);
}

///// DBと接続 /////
function getDBLink() {
    $host = "localhost";
    $userName = "root";
    $passWord = "";
    $dbName = "bill_system";
    $db_link = mysqli_connect($host, $userName, $passWord, $dbName);
    mysqli_set_charset($db_link, 'utf8');
    // 接続状況をチェックします
    if (mysqli_connect_errno()) {
        die("データベースに接続できません:" . mysqli_connect_error() . "\n");
    }
    return $db_link;
}
///// JSON形式に変換して返す ////
function responseJson($list) {
    header("Content-type: application/json; charset=UTF-8");
    echo json_encode($list);
}
?>
