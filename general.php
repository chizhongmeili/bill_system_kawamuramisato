<?php
$JSON_POST = json_decode(file_get_contents("php://input"), true);
$response_list = [];
$db_link = getDBLink();

////一覧の取得///
if ($JSON_POST["actionKind"] === "find") {
    $response_list = findList($db_link, $JSON_POST);
} 
////項目の追加・更新///
else if ($JSON_POST["actionKind"] === "save") {
    $response_list = addList($db_link, $JSON_POST);
} 
////項目の削除///
else if ($JSON_POST["actionKind"] === "delete") {
    $response_list = deleteList($db_link, $JSON_POST);
} 

mysqli_close($db_link);
responseJson($response_list);
exit;




///// 一覧の取得 ////
function findList($db_link, $search_info) {
    $pricelList = [];
    $query = " SELECT "
            ."id, "
            ."name, "
            ."price "
            ."FROM general_price_master ";

            if (isset($search_info["id"]) && $search_info["id"] != null) {
                $query =$query. " WHERE id = " . $search_info["id"] ; 
            } 
        
    $query =$query. " ORDER BY id ASC; " ;
    //  print $query;
     $result = mysqli_query($db_link,$query);
     while ($row = mysqli_fetch_array($result)) {
        $rowmap = [];
        $rowmap['id'] = $row["id"];
        $rowmap['name'] = $row["name"];
        $rowmap['price'] = $row["price"];

        $pricelList[] = $rowmap;
    }
    mysqli_free_result($result);
    return $pricelList;
}


//// 新規保存・更新 ////
function addList($db_link, $JSON_POST) {
    if ( $JSON_POST["id"] != null) {
        $query =" UPDATE general_price_master "
                ." SET "
                ." name = '{$JSON_POST['name']}',"
                ." price = '{$JSON_POST['price']}'"
                ." WHERE id ='{$JSON_POST['id']}';";
    }else {
        $query = " INSERT INTO general_price_master ";
        $query .= " ( ";
        $query .= " name, ";
        $query .= " price ";
        $query .= " ) VALUES ( ";
        $query .= " '{$JSON_POST['name']}', ";
        $query .= " '{$JSON_POST['price']}' ";
        $query .= " ) ";
    }
    $result = mysqli_query($db_link, $query);
    $save_result_info = ["result" => $result];
    if ($result === true) {
        $save_result_info["message"] =  "項目の追加が完了しました。";
    } else {
        $save_result_info["message"] = "SQL実行失敗:".$query;
    }
    return $save_result_info;
}


///// 項目の更新 /////
function updateList($db_link, $JSON_POST) {
    $query =" UPDATE general_price_master ";
    $query .=" SET ";
    $query .=" name = '{$JSON_POST['name']}',";
    $query .=" price = '{$JSON_POST['price']}'";
    $query .=" WHERE id ='{$JSON_POST['id']}';";
    $result = mysqli_query($db_link, $query);
    $save_result_info = ["result" => $result];
    if ($result === true) {
        $save_result_info["message"] =  "更新しました。";
    } else {
        $save_result_info["message"] = "SQL実行失敗:".$query;
    }
    return $save_result_info;
}
///// 項目の削除 /////
function deleteList($db_link, $info) {
    $query = 'DELETE FROM general_price_master WHERE id = '.$info["id"];
    // print $query;
    $result = mysqli_query($db_link, $query);
    $save_result_info = ["result" => $result];
    if ($result === true) {
    //    $〇〇['△△'] = '□□' と $〇〇 = ["△△" => $□□]　は書き方が違うだけで意味は同じ
        $save_result_info["message"] = "削除しました。";
    } else {
        $save_result_info["message"] = "SQL実行失敗:".$query;
    }
    return $save_result_info;
}


///// DBと接続 /////
function getDBLink() {
    $host = "localhost";
    $userName = "root";
    $passWord = "";
    $dbName = "bill_system";
    $db_link = mysqli_connect($host, $userName, $passWord, $dbName);
    mysqli_set_charset($db_link, 'utf8');
    // 接続状況をチェックします
    if (mysqli_connect_errno()) {
        die("データベースに接続できません:" . mysqli_connect_error() . "\n");
    }
    return $db_link;
}
///// JSON形式に変換して返す ////
function responseJson($list) {
    header("Content-type: application/json; charset=UTF-8");
    echo json_encode($list);
}

?>