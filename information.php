<?php 

$JSON_POST = json_decode(file_get_contents("php://input"), true);
$response_list = [];
$db_link = getDBLink();
////生徒情報の取得///
if ($JSON_POST["actionKind"] === "find") {
    $response_list = findStudent($db_link, $JSON_POST);
}   
///校舎の取得///
else if ($JSON_POST["actionKind"] === "school") {
    $response_list = findSchool($db_link);
} 
///会員区分の取得///
else if ($JSON_POST["actionKind"] === "classification") {
    $response_list = findClass($db_link);
} 
///生徒情報の保存///  
else if ($JSON_POST["actionKind"] === "save") {
    $response_list = saveStudent($db_link, $JSON_POST);
}   
///退会生徒の取得///
else if ($JSON_POST["actionKind"] === "quit") {
    $response_list = quitStudent($db_link, $JSON_POST);
}
mysqli_close($db_link);
responseJson($response_list);

exit;



function getDBLink() {
    // DBの接続情報
    $host = "localhost";
    $userName = "root";
    $passWord = "";
    $dbName = "bill_system";
    $db_link = mysqli_connect($host, $userName, $passWord, $dbName);
    mysqli_set_charset($db_link, 'utf8');
    // 接続状況をチェックします
    if (mysqli_connect_errno()) {
        die("データベースに接続できません:" . mysqli_connect_error() . "\n");
    }
    return $db_link;
}


///////// 生徒情報の取得 ////////
function findStudent($db_link, $search_info) {
    $studentList = [];
    $where = [];
    
    $query = " SELECT "
    ."student_table.student_id, "
    ."student_table.last_kanji, "
    ."student_table.first_kanji, "
    ."student_table.last_kana, "
    ."student_table.first_kana, "
    ."student_table.last_name, "
    ."student_table.birth_day, "
    ."student_table.first_name, "
    ."student_table.zip_code, "
    ."student_table.address_1, "
    ."student_table.address_2, "
    ."school_master.school AS school_master, "
    ."student_table.school, "
    ."classification_master.classification AS classification_master, "
    ."student_table.classification, "
    ."student_table.benefit, "
    ."student_table.max_limit, "
    ."student_table.renewal, "
    ."student_table.joined, "
    ."student_table.quit "
    ."FROM student_table "
    ."JOIN school_master ON student_table.school = school_master.school_id "
    ."JOIN classification_master ON student_table.classification = classification_master.classification_id" ;
    ////// IDで検索する時
    if (isset($search_info["id"]) && $search_info["id"] != null) {
        $where[] = " student_table.student_id = " . $search_info["id"]; 
    } 
    ////// 名前で検索
    if (isset($search_info["studentName"]) && $search_info["studentName"] != null) {
        $where[] = " concat(student_table.last_kanji,student_table.first_kanji) like '%".$search_info["studentName"]."%'";
    }
    ////// 校舎で検索 
    if (isset($search_info["studentSchool"]) && $search_info["studentSchool"] != null && $search_info["studentSchool"] != '選択してください') {
        $where[] = " school_master.school_id = '" . $search_info["studentSchool"] . "'";
    }
    ////// 在校生を検索
    if (isset($search_info["studentsKind"]) && $search_info["studentsKind"] != null && $search_info["studentsKind"] === '在校生徒') {
        $where[] = " (student_table.quit = '' OR student_table.quit IS NULL OR date_format(now(), '%Y-%m') <= student_table.quit )"; 
    } 
    ////// 退会生徒を検索
    if (isset($search_info["studentsKind"]) && $search_info["studentsKind"] != null && $search_info["studentsKind"] === '退会生徒') {
        $where[] = " date_format(now(), '%Y-%m') > student_table.quit  AND ( student_table.quit != '' AND student_table.quit IS NOT NULL) ";
    } 
    for ($i = 0; $i < count($where); $i++) {
        $value = $where[$i];
        if ($i === 0) {
            $query = $query. ' WHERE ' .$value;
        } else if ($i >= 1) {
            $query = $query. ' AND ' .$value;
        }
    }   
    $query = $query." ORDER BY student_id ASC;";
    //  print $query;
    // クエリの実行
    $result = mysqli_query($db_link,$query);
    // var_dump($result) ;
    while ($row = mysqli_fetch_array($result)) {
        $birth = str_replace('-', '', $row["birth_day"]);
        $rowMap = [];
        
        $rowMap["studentId"] = $row["student_id"];
        $rowMap["school"] = $row["school"];
        $rowMap["school_master"] = $row["school_master"];
        $rowMap["lastKanji"] = $row["last_kanji"];
        $rowMap["firstKanji"] = $row["first_kanji"];
        $rowMap["lastKana"] = $row["last_kana"];
        $rowMap["firstKana"] = $row["first_kana"];
        $rowMap["lastName"] = $row["last_name"];
        $rowMap["firstName"] = $row["first_name"];
        $rowMap["birthDay"] = $row["birth_day"];
        $rowMap["zipCode"] = $row["zip_code"];
        $rowMap["address1"] = $row["address_1"];
        $rowMap["address2"] = $row["address_2"];
        $rowMap["benefit"] = $row["benefit"];
        $rowMap["classification"] = $row["classification"];
        $rowMap["classification_master"] = $row["classification_master"];
        $rowMap["maxLimit"] = $row["max_limit"];
        $rowMap["renewal"] = $row["renewal"];
        $rowMap["joined"] = $row["joined"];
        if ($row["quit"] === NULL) {
            $rowMap["quit"] = "" ;
        } else {
            $rowMap["quit"] = $row["quit"];
        }
        $rowMap["grade"] = grade($birth);
    
        $studentList[] = $rowMap;
    }
    mysqli_free_result($result);
    return $studentList;
}

///// 校舎の取得 /////
function findSchool($db_link) {
    $query = " SELECT school_id, school FROM school_master;";
    $result = mysqli_query($db_link,$query);
    
    while ($row = mysqli_fetch_array($result)) {
        $rowMap = [];
        $rowMap["school_id"] = $row["school_id"];
        $rowMap["school"] = $row["school"];
        
        $schoolList[] = $rowMap;
    }
    mysqli_free_result($result);

    return $schoolList;
}

///// 会員区分の取得 /////
function findClass($db_link) {
    // $studentList = [];
    $query = " SELECT classification_id, classification FROM classification_master;";
    $result = mysqli_query($db_link,$query);
    while ($row = mysqli_fetch_array($result)) {
        $rowMap = [];
        $rowMap["class_id"] = $row["classification_id"];
        $rowMap["class"] = $row["classification"];
        
        $classList[] = $rowMap;
    }
    mysqli_free_result($result);
    return $classList;
}


/////  生徒情報の保存 //////
function saveStudent($db_link, $save_info) {
    $query = "";
    $renewal = zeroRenewal($save_info['renewal']);
    $joined = zeroPadding($save_info['joined']);
    if ($save_info["id"] != null) {
        if (isset($save_info['quit']) && $save_info['quit'] != null) {
            $quit = zeroPadding($save_info['quit']);
        } else {
            $quit = NULL ;
        }
        $query .=" UPDATE student_table";
        $query .=" SET";
        // $query .=" last_kanji =    '".$save_info['lastKanji']."',";ここと下の書き方は同じ意味。変数展開
        $query .=" last_kanji =    '{$save_info['lastKanji']}',";
        $query .=" first_kanji =   '{$save_info['firstKanji']}',";
        $query .=" last_kana =     '{$save_info['lastKana']}',";
        $query .=" first_kana =    '{$save_info['firstKana']}',";
        $query .=" last_name =     '{$save_info['lastName']}',";
        $query .=" first_name =    '{$save_info['firstName']}',";
        $query .=" birth_day =     '{$save_info['birthDay']}',";
        $query .=" zip_code =      '{$save_info['zipCode']}',";
        $query .=" address_1 =     '{$save_info['address1']}',";
        $query .=" address_2 =     '{$save_info['address2']}',";
        $query .=" benefit =       '{$save_info['benefit']}',";
        $query .=" max_limit =     '{$save_info['maxLimit']}',";
        $query .=" school =        '{$save_info['school']}',";
        $query .=" classification ='{$save_info['classification']}',";
        $query .=" joined =        '$joined',";
        $query .=" renewal =       '$renewal',";
        $query .=" quit  =       '$quit'";
        $query .=" WHERE student_id = {$save_info['id']};";
    } else {
        $query .="INSERT INTO student_table";
        $query .=" (";
        $query .=" last_kanji,";
        $query .=" first_kanji,";
        $query .=" last_kana,";
        $query .=" first_kana,";
        $query .=" last_name,";
        $query .=" first_name,";
        $query .=" birth_day,";
        $query .=" zip_code,";
        $query .=" address_1,";
        $query .=" address_2,";
        $query .=" benefit,";
        $query .=" max_limit,";
        $query .=" school,";
        $query .=" classification,";
        $query .=" renewal,";
        $query .=" joined";
        $query .=" ) VALUES (";
        $query .=" '{$save_info['lastKanji']}',";
        $query .=" '{$save_info['firstKanji']}',";
        $query .=" '{$save_info['lastKana']}',";
        $query .=" '{$save_info['firstKana']}',";
        $query .=" '{$save_info['lastName']}',";
        $query .=" '{$save_info['firstName']}',";
        $query .=" '{$save_info['birthDay']}',";
        $query .=" '{$save_info['zipCode']}',";
        $query .=" '{$save_info['address1']}',";
        $query .=" '{$save_info['address2']}',";
        $query .=" '{$save_info['benefit']}',";
        $query .=" '{$save_info['maxLimit']}',";
        $query .=" '{$save_info['school']}',";
        $query .=" '{$save_info['classification']}',";
        $query .=" '$renewal',";
        $query .=" '$joined'";
        $query .= " );";
    }
        $result = mysqli_query($db_link, $query);
        // print $query;
        // ["result" => $result]PHPの連想配列の書き方。
        $save_result_info = ["result" => $result];
        if ($result === true) {
        //    $〇〇['△△'] = '□□' と $〇〇 = ["△△" => $□□]　は書き方が違うだけで意味は同じ
            $save_result_info["message"] = "生徒情報の保存が完了しました。";
        } else {
            $save_result_info["message"] = "SQL実行失敗:".$query;
        }
        return $save_result_info;
}


/////// 退会情報の保存 //////
function quitStudent($db_link, $save_info) {
    $quit = zeroPadding($save_info['quit']);
    $query = " UPDATE student_table "
            ." SET "
            ." quit = '$quit' "
            ." WHERE student_id = {$save_info['id']};";
    // print $query;
    $result = mysqli_query($db_link, $query);
    // ["result" => $result]PHPの連想配列の書き方。
    $save_result_info = ["result" => $result];
    if ($result === true) {
    //    $〇〇['△△'] = '□□'　と　$〇〇 = ["△△" => $□□]　は書き方が違うだけで意味は同じ
        $save_result_info["message"] = "退会情報の保存が完了しました。";
    } else {
        $save_result_info["message"] = "SQL実行失敗:".$query;
    }
    return $save_result_info;
}




function responseJson($list) {
    header("Content-type: application/json; charset=UTF-8");
    echo json_encode($list);
}

///// ゼロ埋め(更新月) /////
function zeroRenewal($data) {
    if (preg_match('/^[0][1-9]|1[0-2]$/', $data) === 0 ){
        return '0' . $data ;
    } return $data;
   
}
//// ゼロ埋め(入会、退会)) ////
function zeroPadding($data) {
        if(preg_match('/^(20[0-9]{2})-[0][1-9]|1[0-2]$/', $data) === 0) {
            $a = substr($data, 0, 5);
            $b = '0';
            $c = substr($data, 5, 1);
            return $a . $b .$c ;
        } else {
            return $data;
        }
    }

////// 学年を出す関数 //////
function grade($birth){
    //今日の日付を取得
    $now = date('Ymd');
    //年齢の計算
    $age = floor(($now-$birth)/10000);
    //各月日を求める
    $birth_y = substr($birth, 0, 4); 
    $birth_m = substr($birth, 4, 4);
    $now_y = substr($now, 0, 4);
    $now_m = substr($now, 4, 4);
    
    if ($now_m < 400) { //前学期 
        $m = 7;   
    } else { //新学期
        $m = 6;  
    }
    
    if($birth_m < 402) { //早生まれ  
        $now_y++;   //いまの年に1足してる 2021→2022
    }
    //学年の計算
    $grade = $now_y - $birth_y - $m;
    //結果を返す
    if($grade>=1 && $grade<=6){
        $data = $grade."年生";
    }else if ($grade === 0) {
        $data = '年長';
    } else {
        $data = $age."歳";
    }
    return $data;
}
    
?>
