<?php
$JSON_POST = json_decode(file_get_contents("php://input"), true);
$response_list = [];
$db_link = getDBLink();

////一律利用料金の取得///
if ($JSON_POST["actionKind"] === "findFee") {
    $response_list = findFeeList($db_link, $JSON_POST);
} 
////月謝一覧取得///
else if ($JSON_POST["actionKind"] === "find") {
    $response_list = findList($db_link, $JSON_POST);
} 
//// 料金プラン取得 ////
else if ($JSON_POST["actionKind"] === "plan") {
    $response_list = findType($db_link, $JSON_POST);
}
//// 曜日取得 ////
else if ($JSON_POST["actionKind"] === "day") {
    $response_list = findDay($db_link, $JSON_POST);
}
////項目の追加・更新///
else if ($JSON_POST["actionKind"] === "save") {
    $response_list = addFee($db_link, $JSON_POST);
} 
//// 一律利用料金の削除 ////
else if ($JSON_POST["actionKind"] === "deleteFee") {
    $response_list = deleteFeeData($db_link,$JSON_POST);
} 
//// 月謝プランの削除 ////
else if ($JSON_POST["actionKind"] === "deletePlan") {
    $response_list = deleteMonthlyData($db_link,$JSON_POST);
} 
mysqli_close($db_link);
responseJson($response_list);
exit;



///// 月謝プラン一覧の取得 ////
function findList($db_link, $search_info) {
    $pricelList = [];
    $where = [];
    $query = " SELECT "
            ."regular_monthlyFee_master.id AS monthly_id, "
            ."regular_monthlyFee_master.name, "
            ."regular_monthlyFee_master.price, "
            ."regular_plan_type.id AS plan_type, "
            ."dayofweek.id AS day_id, "
            ."dayofweek.day "
            ."FROM regular_monthlyFee_master "
            ." LEFT JOIN regular_plan_type ON regular_monthlyFee_master.type = regular_plan_type.id "
            ."LEFT JOIN dayofweek ON regular_monthlyFee_master.day = dayofweek.id ";

            if (isset($search_info["id"]) && $search_info["id"] != null) {
                $query = $query.' WHERE regular_monthlyFee_master.id = "' .$search_info["id"]. '"';
            }
    
    $query = $query." ORDER BY regular_monthlyFee_master.id ASC;";
    //  print $query;
     $result = mysqli_query($db_link,$query);
     while ($row = mysqli_fetch_array($result)) {
        $rowmap = [];
        $rowmap['id'] = $row["monthly_id"];
        $rowmap['name'] = $row["name"];
        $rowmap['price'] = $row["price"];
        $rowmap['day'] = $row["day"];
        $rowmap['day_id'] = $row["day_id"];
        $rowmap['type'] = $row["plan_type"];

        $pricelList[] = $rowmap;
    }
    mysqli_free_result($result);
    return $pricelList;
}

//// 一律利用料金一覧の取得 ////
function findFeeList($db_link, $search_info) {
    $list = [];
// var_dump($search_info);
    $query = " SELECT "
            ." id, "
            ." name, "
            ." price, "
            ." type "
            ." FROM regular_price_master ";
            
            if (isset($search_info["type"]) && $search_info["type"] != null) {
                $query =$query." WHERE type = '' OR type IS NULL ";
            }
            if (isset($search_info["id"]) && $search_info["id"] != null) {
                $query =$query." WHERE id = " .$search_info["id"];
            }
            
    $query = $query." ORDER BY id ASC;";
    // print $query;
    $result = mysqli_query($db_link,$query);
    while ($row = mysqli_fetch_array($result)) {
        $rowmap = [];
        $rowmap['id'] = $row["id"];
        $rowmap['name'] = $row["name"];
        $rowmap['price'] = $row["price"];

        $list[] = $rowmap;
    }
    mysqli_free_result($result);
    return $list;
}

//// 月謝の種類取得 ////
function findType($db_link) {
    $query = "SELECT id, type FROM regular_plan_type;";
    $result = mysqli_query($db_link,$query);
    
    while ($row = mysqli_fetch_array($result)) {
        $rowMap = [];
        $rowMap["id"] = $row["id"];
        $rowMap["type"] = $row["type"];
        
        $typeList[] = $rowMap;
    }
    mysqli_free_result($result);

    return $typeList;
}

//// 曜日の取得 ////
function findDay($db_link) {
    $query = " SELECT id, day FROM dayofweek ; ";
    $result = mysqli_query($db_link,$query);
    
    while ($row = mysqli_fetch_array($result)) {
        $rowMap = [];
        $rowMap["id"] = $row["id"];
        $rowMap["day"] = $row["day"];
        
        $typeList[] = $rowMap;
    }
    mysqli_free_result($result);

    return $typeList;
}

//// 項目の追加 ////
function addFee($db_link, $JSON_POST) {
    if ( $JSON_POST["id"] != null) {
        if ($JSON_POST["type"] === "1") {
            $query = " UPDATE regular_price_master ";
            $query .= " SET ";
            $query .= " name = '{$JSON_POST['name']}', ";
            $query .= " price = '{$JSON_POST['price']}' ";
            $query .= " WHERE id = {$JSON_POST['id']}; ";
            // ↑なんで””の中にシングルクオートがいるの文字列だから？94行目は波かっこもない
        } else if ($JSON_POST["type"] === "2") {
            $query = " UPDATE regular_monthlyfee_master ";
            $query .= " SET ";
            $query .= " name = '{$JSON_POST['name']}', ";
            $query .= " price = '{$JSON_POST['price']}', ";
            $query .= " type = {$JSON_POST['type']} ";
            $query .= " WHERE id = {$JSON_POST['id']}; ";
        } else if ($JSON_POST["type"] === "3") {
            $query = " UPDATE regular_monthlyfee_master ";
            $query .= " SET ";
            $query .= " name = '{$JSON_POST['name']}', ";
            $query .= " price = '{$JSON_POST['price']}', ";
            $query .= " type = {$JSON_POST['type']} ";
            $query .= " WHERE id = {$JSON_POST['id']}; ";
        } else if ($JSON_POST["type"] === "4") {
            $query = " UPDATE regular_monthlyfee_master ";
            $query .= " SET ";
            $query .= " name = '{$JSON_POST['name']}', ";
            $query .= " price = '{$JSON_POST['price']}', ";
            $query .= " type = {$JSON_POST['type']} ";
            $query .= " WHERE id = {$JSON_POST['id']}; ";
        } else if ($JSON_POST["type"] === "5") {
            $query = " UPDATE regular_monthlyfee_master ";
            $query .= " SET ";
            $query .= " name = '{$JSON_POST['name']}', ";
            $query .= " price = '{$JSON_POST['price']}', ";
            $query .= " type = {$JSON_POST['type']} ";
            $query .= " WHERE id = {$JSON_POST['id']}; ";
        } else if ($JSON_POST["type"] === "6") {
            $query = " UPDATE regular_monthlyfee_master ";
            $query .= " SET ";
            $query .= " name = '{$JSON_POST['name']}', ";
            $query .= " price = '{$JSON_POST['price']}', ";
            $query .= " type = {$JSON_POST['type']} ";
            $query .= " WHERE id = {$JSON_POST['id']}; ";
        } else if ($JSON_POST["type"] === "7") {
            $query = " UPDATE regular_monthlyfee_master ";
            $query .= " SET ";
            $query .= " name = '{$JSON_POST['name']}', ";
            $query .= " price = '{$JSON_POST['price']}', ";
            $query .= " type = {$JSON_POST['type']} ";
            $query .= " WHERE id = {$JSON_POST['id']}; ";
        } else if ($JSON_POST["type"] === "8") {
            $query = " UPDATE regular_monthlyfee_master ";
            $query .= " SET ";
            $query .= " name = '{$JSON_POST['name']}', ";
            $query .= " price = '{$JSON_POST['price']}', ";
            $query .= " type = {$JSON_POST['type']}, ";
            $query .= " day = {$JSON_POST['day']} ";
            $query .= " WHERE id = {$JSON_POST['id']}; ";
        }
    }else {
        if ($JSON_POST["type"] === '1') {
            $query = " INSERT INTO regular_price_master ";
            $query .= " ( ";
            $query .= " name, ";
            $query .= " price ";
            $query .= " ) VALUES ( ";
            $query .= " '{$JSON_POST['name']}', ";
            // ↑なんで””の中にシングルクオートがいるの？
            $query .= " '{$JSON_POST['price']}' ";
            $query .= " ) ";
        } else if ($JSON_POST["type"] === '2') {
            $query = " INSERT INTO regular_monthlyfee_master ";
            $query .= " ( ";
            $query .= " name, ";
            $query .= " price, ";
            $query .= " type ";
            $query .= " ) VALUES ( ";
            $query .= " '{$JSON_POST['name']}', ";
            $query .= " '{$JSON_POST['price']}', ";
            $query .= " '{$JSON_POST['type']}' ";
            $query .= " ) ";
        } else if ($JSON_POST["type"] === '3') {
            $query = " INSERT INTO regular_monthlyfee_master ";
            $query .= " ( ";
            $query .= " name, ";
            $query .= " price, ";
            $query .= " type ";
            $query .= " ) VALUES ( ";
            $query .= " '{$JSON_POST['name']}', ";
            $query .= " '{$JSON_POST['price']}', ";
            $query .= " '{$JSON_POST['type']}' ";
            $query .= " ) ";
        } else if ($JSON_POST["type"] === '4') {
            $query = " INSERT INTO regular_monthlyfee_master ";
            $query .= " ( ";
            $query .= " name, ";
            $query .= " price, ";
            $query .= " type ";
            $query .= " ) VALUES ( ";
            $query .= " '{$JSON_POST['name']}', ";
            $query .= " '{$JSON_POST['price']}', ";
            $query .= " '{$JSON_POST['type']}' ";
            $query .= " ) ";
        } else if ($JSON_POST["type"] === '5') {
            $query = " INSERT INTO regular_monthlyfee_master ";
            $query .= " ( ";
            $query .= " name, ";
            $query .= " price, ";
            $query .= " type ";
            $query .= " ) VALUES ( ";
            $query .= " '{$JSON_POST['name']}', ";
            $query .= " '{$JSON_POST['price']}', ";
            $query .= " '{$JSON_POST['type']}' ";
            $query .= " ) ";
        } else if ($JSON_POST["type"] === '6') {
            $query = " INSERT INTO regular_monthlyfee_master ";
            $query .= " ( ";
            $query .= " name, ";
            $query .= " price, ";
            $query .= " type ";
            $query .= " ) VALUES ( ";
            $query .= " '{$JSON_POST['name']}', ";
            $query .= " '{$JSON_POST['price']}', ";
            $query .= " '{$JSON_POST['type']}' ";
            $query .= " ) ";
        } else if ($JSON_POST["type"] === '7') {
            $query = " INSERT INTO regular_monthlyfee_master ";
            $query .= " ( ";
            $query .= " name, ";
            $query .= " price, ";
            $query .= " type ";
            $query .= " ) VALUES ( ";
            $query .= " '{$JSON_POST['name']}', ";
            $query .= " '{$JSON_POST['price']}', ";
            $query .= " '{$JSON_POST['type']}' ";
            $query .= " ) ";
        } else if ($JSON_POST["type"] === '8') {
            $query = " INSERT INTO regular_monthlyfee_master ";
            $query .= " ( ";
            $query .= " name, ";
            $query .= " price, ";
            $query .= " type, ";
            $query .= " day ";
            $query .= " ) VALUES ( ";
            $query .= " '{$JSON_POST['name']}', ";
            $query .= " '{$JSON_POST['price']}', ";
            $query .= " '{$JSON_POST['type']}', ";
            $query .= " '{$JSON_POST['day']}' ";
            $query .= " ) ";
        }
    }
    $result = mysqli_query($db_link, $query);
    $save_result_info = ["result" => $result];
    if ($result === true) {
        $save_result_info["message"] =  "項目の追加が完了しました。";
    } else {
        $save_result_info["message"] = "SQL実行失敗:".$query;
    }
    return $save_result_info;
    
}


//// 一律利用料金の削除 ////
function deleteFeeData($db_link, $JSON_POST) {
    $query = 'DELETE FROM regular_price_master WHERE id = '.$JSON_POST["id"];
    // print $query;
    $result = mysqli_query($db_link, $query);
    $save_result_info = ["result" => $result];
    if ($result === true) {
    //    $〇〇['△△'] = '□□' と $〇〇 = ["△△" => $□□]　は書き方が違うだけで意味は同じ
        $save_result_info["message"] = "削除しました。";
    } else {
        $save_result_info["message"] = "SQL実行失敗:".$query;
    }
    return $save_result_info;
}

//// 月謝プランの削除 ////
function deleteMonthlyData($db_link, $JSON_POST) {
    $query = 'DELETE FROM regular_monthlyfee_master WHERE id = '.$JSON_POST["id"];
    // print $query;
    $result = mysqli_query($db_link, $query);
    $save_result_info = ["result" => $result];
    if ($result === true) {
    //    $〇〇['△△'] = '□□' と $〇〇 = ["△△" => $□□]　は書き方が違うだけで意味は同じ
        $save_result_info["message"] = "削除しました。";
    } else {
        $save_result_info["message"] = "SQL実行失敗:".$query;
    }
    return $save_result_info;
}



///// DBと接続 /////
function getDBLink() {
    $host = "localhost";
    $userName = "root";
    $passWord = "";
    $dbName = "bill_system";
    $db_link = mysqli_connect($host, $userName, $passWord, $dbName);
    mysqli_set_charset($db_link, 'utf8');
    // 接続状況をチェックします
    if (mysqli_connect_errno()) {
        die("データベースに接続できません:" . mysqli_connect_error() . "\n");
    }
    return $db_link;
}
///// JSON形式に変換して返す ////
function responseJson($list) {
    header("Content-type: application/json; charset=UTF-8");
    echo json_encode($list);
}

?>