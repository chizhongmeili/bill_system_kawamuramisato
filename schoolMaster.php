<?php
$JSON_POST = json_decode(file_get_contents("php://input"), true);
$response_list = [];
$db_link = getDBLink();

////校舎情報の取得///
if ($JSON_POST["actionKind"] === "school") {
    $response_list = findSchool($db_link, $JSON_POST);
} 
////校舎の追加///
else if ($JSON_POST["actionKind"] === "save") {
    $response_list = addSchool($db_link, $JSON_POST);
} 
//// 校舎の削除 ////
else if ($JSON_POST["actionKind"] === "delete") {
    $response_list = deleteSchool($db_link,$JSON_POST);
} 

mysqli_close($db_link);
responseJson($response_list);
exit;

///// 校舎情報の取得 ////
function findSchool($db_link, $search_info) {
    $schoolList = [];
    $query = " SELECT "
            ."school_id, "
            ."school, "
            ."phone_number, "
            ."fax, "
            ."address "
            ."FROM school_master "
            ."ORDER BY school_id ASC; " ;
    //  print $query;
     $result = mysqli_query($db_link,$query);
     while ($row = mysqli_fetch_array($result)) {
        $rowmap = [];

        $rowmap['school_id'] = $row["school_id"];
        $rowmap['school'] = $row["school"];
        $rowmap['phone_number'] = $row["phone_number"];
        $rowmap['fax'] = $row["fax"];
        $rowmap['address'] = $row["address"];

        $schoolList[] = $rowmap;
    }
    mysqli_free_result($result);
    return $schoolList;
}

//// 校舎の追加 ////
function addSchool($db_link, $JSON_POST) {
    $query = " INSERT INTO school_master ";
    $query .= " ( ";
    $query .= " school, ";
    $query .= " address, ";
    $query .= " phone_number, ";
    $query .= " fax ";
    $query .= " ) VALUES ( ";
    $query .= " '{$JSON_POST['school']}', ";
    $query .= " '{$JSON_POST['address']}', ";
    $query .= " '{$JSON_POST['phone_number']}', ";
    $query .= " '{$JSON_POST['fax']}' ";
    $query .= " ) ";

    $result = mysqli_query($db_link, $query);
    $save_result_info = ["result" => $result];
    if ($result === true) {
        $save_result_info["message"] =  "校舎の追加が完了しました。";
    } else {
        $save_result_info["message"] = "SQL実行失敗:".$query;
    }
    return $save_result_info;
    
}
//// 校舎の削除 ////
function deleteSchool($db_link, $JSON_POST) {
    $query = 'DELETE FROM school_master WHERE school_id = '.$JSON_POST["id"];
    // print $query;
    $result = mysqli_query($db_link, $query);
    $save_result_info = ["result" => $result];
    if ($result === true) {
    //    $〇〇['△△'] = '□□' と $〇〇 = ["△△" => $□□]　は書き方が違うだけで意味は同じ
        $save_result_info["message"] = "削除しました。";
    } else {
        $save_result_info["message"] = "SQL実行失敗:".$query;
    }
    return $save_result_info;
}
///// DBと接続 /////
function getDBLink() {
    $host = "localhost";
    $userName = "root";
    $passWord = "";
    $dbName = "bill_system";
    $db_link = mysqli_connect($host, $userName, $passWord, $dbName);
    mysqli_set_charset($db_link, 'utf8');
    // 接続状況をチェックします
    if (mysqli_connect_errno()) {
        die("データベースに接続できません:" . mysqli_connect_error() . "\n");
    }
    return $db_link;
}
///// JSON形式に変換して返す ////
function responseJson($list) {
    header("Content-type: application/json; charset=UTF-8");
    echo json_encode($list);
}

?>