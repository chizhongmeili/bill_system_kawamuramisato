<?php 
$JSON_POST = json_decode(file_get_contents("php://input"), true);
$response_list = [];
$db_link = getDBLink();

////// ユーザーの取得 /////
if ($JSON_POST["actionKind"] === "user") {
    $response_list = findUser($db_link);
}
///校舎の取得///
else if ($JSON_POST["actionKind"] === "school") {
    $response_list = findSchool($db_link);
} 
//// ユーザーの登録 ////
else if ($JSON_POST["actionKind"] === "save_user") {
    $response_list = saveUser($db_link,$JSON_POST);
} 
//// ユーザーの削除 ////
else if ($JSON_POST["actionKind"] === "delete") {
    $response_list = deleteUser($db_link,$JSON_POST);
} 

mysqli_close($db_link);
responseJson($response_list);

///// ユーザーの取得 ////
function findUser($db_link) {
    $userList = [];

    $query = " SELECT "
            ." user_table.user_id, "
            ." user_table.user_name, "
            ." user_table.school_id, "
            ." school_master.school_id, "
            ." school_master.school, "
            ." user_table.user_kind "
            ." FROM user_table "
            ." JOIN school_master ON user_table.school_id = school_master.school_id "
            ." ORDER BY user_table.user_id ASC; " ;
    // print $query;  
    $result = mysqli_query($db_link,$query);
    while ($row = mysqli_fetch_array($result)) {
        $rowmap = [];

        $rowmap['user_id'] = $row["user_id"];
        $rowmap['user_name'] = $row["user_name"];
        $rowmap['school'] = $row["school"];
        $rowmap['kind'] = $row["user_kind"];

        $userList[] = $rowmap;
    }
    mysqli_free_result($result);
    return $userList;
}

//// ユーザーの登録 ////
function saveUser($db_link, $save_info) {
    $query  = " INSERT INTO user_table ";
    $query .= " ( ";
    $query .= " user_name, ";
    $query .= " school_id, ";
    $query .= " password, ";
    $query .= " user_kind ";
    $query .= " ) VALUES ( ";
    $query .= " '{$save_info['user_name']}', ";
    $query .= " '{$save_info['school']}', ";
    $query .= " '{$save_info['password']}', ";
    $query .= " '{$save_info['kind']}' ";
    $query .= " )";
   
    $result = mysqli_query($db_link, $query);
    
    $save_result_info = ["result" => $result];
    if ($result === true) {
        $save_result_info["message"] =  "ユーザーの保存が完了しました。";
    } else {
        $save_result_info["message"] = "SQL実行失敗:".$query;
    }
    return $save_result_info;
    

}

///// 校舎の取得 /////
function findSchool($db_link) {
    $query = " SELECT school_id, school FROM school_master;";
    $result = mysqli_query($db_link,$query);
    
    while ($row = mysqli_fetch_array($result)) {
        $rowMap = [];
        $rowMap["school_id"] = $row["school_id"];
        $rowMap["school"] = $row["school"];
        
        $schoolList[] = $rowMap;
    }
    mysqli_free_result($result);

    return $schoolList;
}

///// ユーザーの削除 /////
function deleteUser($db_link, $info) {
    $query = 'DELETE FROM user_table WHERE user_id = '.$info["id"];
    // print $query;
    $result = mysqli_query($db_link, $query);
    $save_result_info = ["result" => $result];
    if ($result === true) {
    //    $〇〇['△△'] = '□□' と $〇〇 = ["△△" => $□□]　は書き方が違うだけで意味は同じ
        $save_result_info["message"] = "削除しました。";
    } else {
        $save_result_info["message"] = "SQL実行失敗:".$query;
    }
    return $save_result_info;
}


///// DBと接続 /////
function getDBLink() {
    $host = "localhost";
    $userName = "root";
    $passWord = "";
    $dbName = "bill_system";
    $db_link = mysqli_connect($host, $userName, $passWord, $dbName);
    mysqli_set_charset($db_link, 'utf8');
    // 接続状況をチェックします
    if (mysqli_connect_errno()) {
        die("データベースに接続できません:" . mysqli_connect_error() . "\n");
    }
    return $db_link;
}
///// JSON形式に変換して返す ////
function responseJson($list) {
    header("Content-type: application/json; charset=UTF-8");
    echo json_encode($list);
}

?>